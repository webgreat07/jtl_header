
{if $menuSingleRow}
{block name='layout-header-head-resources-crit' append}
    .main-search-wrapper {
        max-width: {$menuSearchWidth};
    }
    .nav-logo-wrapper {
        margin-right: auto;
    }
    @media (min-width: 992px) {
    {if $menuLogoHeight !== 0 && $nSeitenTyp !== $smarty.const.PAGE_BESTELLVORGANG}
        header .navbar-brand img {
            height: {$menuLogoHeight};
        }
    {/if}
    {if $menuSearchPosition === 'right'}
        .main-search-wrapper {
            margin-right: {$menuSearchMarginRight};
        }
    {elseif $menuSearchPosition === 'center'}
        .main-search-wrapper {
            margin-right: auto;
        }
    {elseif $menuSearchPosition === 'left'}
        .main-search-wrapper {
            margin-right: auto;
        }
        .nav-logo-wrapper {
            margin-right: initial;
        }
    {/if}
    }
    {if $menuCentered}
        {if $menuMultipleRows}
            .nav-scrollbar-inner {
                justify-content: center;
            }
        {else}
            .nav-scrollbar-inner::before, .nav-scrollbar-inner::after {
                content: '';
                margin: auto;
            }
        {/if}
    {/if}
{/block}
{block name='layout-header-header'}
    {if !$menuScroll}
        {include file='topbar.tpl'}
    {/if}
    <header class="d-print-none full-width-mega {if !$isMobile || $Einstellungen.template.theme.mobile_search_type !== 'fixed'}sticky-top{/if} fixed-navbar theme-{$Einstellungen.template.theme.theme_default}"
            id="jtl-nav-wrapper">
        {if $menuScroll}
            {block name='layout-header-branding-top-bar'}
                {include file='topbar.tpl'}
            {/block}
        {/if}
        <div class="container-fluid hide-navbar {if $Einstellungen.template.megamenu.header_full_width === 'N'}container-fluid-xl{/if}">
            {navbar toggleable=true fill=true type="expand-lg" class="row justify-content-center align-items-center-util"}
                {col class="col-auto nav-logo-wrapper"}
                    <div class="toggler-logo-wrapper">
                        {block name='layout-header-navbar-toggle'}
                            <button id="burger-menu" class="burger-menu-wrapper navbar-toggler collapsed {if $nSeitenTyp === $smarty.const.PAGE_BESTELLVORGANG}d-none{/if}" type="button" data-toggle="collapse" data-target="#mainNavigation" aria-controls="mainNavigation" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        {/block}

                        {block name='layout-header-logo'}
                            <div id="logo" class="logo-wrapper" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
                                <span itemprop="name" class="d-none">{$meta_publisher}</span>
                                <meta itemprop="url" content="{$ShopHomeURL}">
                                <meta itemprop="logo" content="{$ShopLogoURL}">
                                {link class="navbar-brand" href=$ShopHomeURL title=$Einstellungen.global.global_shopname}
                                {if isset($ShopLogoURL)}
                                    {image src=$ShopLogoURL
                                    alt=$Einstellungen.global.global_shopname
                                    id="shop-logo"
                                    }
                                {else}
                                    <span class="h1">{$Einstellungen.global.global_shopname}</span>
                                {/if}
                                {/link}
                            </div>
                        {/block}
                    </div>
                {/col}
                {if $nSeitenTyp !== $smarty.const.PAGE_BESTELLVORGANG}
                    {col class="main-search-wrapper nav-right"}
                        {block name='layout-header-jtl-header-include-header-nav-search'}
                            {include file='layout/header_nav_search.tpl'}
                        {/block}
                    {/col}
                {/if}

                {if $nSeitenTyp === $smarty.const.PAGE_BESTELLVORGANG}
                    {col class="secure-checkout-icon"}
                        {block name='layout-header-secure-checkout-title'}
                            <i class="fas fa-lock icon-mr-2"></i>{lang key='secureCheckout' section='checkout'}
                        {/block}
                    {/col}
                    {col class="secure-checkout-topbar col-auto ml-auto-util d-none d-lg-block"}
                        {block name='layout-header-secure-include-header-top-bar'}
                            {include file='layout/header_top_bar.tpl'}
                        {/block}
                    {/col}
                {else}
                    {col class="col-auto nav-icons-wrapper"}
                        {nav id="shop-nav" right=true class="nav-right order-lg-last nav-icons"}
                            {block name='layout-header-branding-shop-nav-include-language-dropdown'}
                                {include file='snippets/language_dropdown.tpl' dropdownClass='d-flex d-lg-none'}
                            {/block}
                            {include file='layout/header_nav_icons.tpl'}
                        {/nav}
                    {/col}
                {/if}
            {/navbar}
        </div>
        {if $nSeitenTyp !== $smarty.const.PAGE_BESTELLVORGANG}
            <div class="container-fluid {if $Einstellungen.template.megamenu.header_full_width === 'N'}container-fluid-xl{/if}">
                {navbar toggleable=true fill=true type="expand-lg" class="justify-content-start {if $nSeitenTyp === $smarty.const.PAGE_BESTELLVORGANG}align-items-center-util{else}align-items-lg-end{/if}"}
                    <div id="mainNavigation" class="collapse navbar-collapse {if $menuMultipleRows}nav-multiple-row{else}nav-scrollbar{/if}">
                        <div class="nav-mobile-header d-lg-none">
                            {row class="align-items-center-util"}
                            {col class="nav-mobile-header-toggler"}
                                {block name='layout-header-include-categories-mega-toggler'}
                                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#mainNavigation" aria-controls="mainNavigation" aria-expanded="false" aria-label="Toggle navigation">
                                        <span class="navbar-toggler-icon"></span>
                                    </button>
                                {/block}
                            {/col}
                            {col class="col-auto nav-mobile-header-name ml-auto-util"}
                                <span class="nav-offcanvas-title">{lang key='menuName'}</span>
                                {block name='layout-header-include-categories-mega-back'}
                                    {link href="#" class="nav-offcanvas-title d-none" data=["menu-back"=>""]}
                                        <span class="fas fa-chevron-left icon-mr-2"></span>
                                        <span>{lang key='back'}</span>
                                    {/link}
                                {/block}
                            {/col}
                            {/row}
                            <hr class="nav-mobile-header-hr" />
                        </div>
                        <div class="nav-mobile-body">
                            {navbarnav class="nav-scrollbar-inner"}
                                {if $menuScroll}
                                    {block name='layout-header-jtl-header-include-include-categories-mega-home'}
                                        <li class="nav-home-button nav-item nav-scrollbar-item d-none">
                                            {link class="nav-link" href=$ShopURL title=$Einstellungen.global.global_shopname}
                                                <span class="fas fa-home"></span>
                                            {/link}
                                        </li>
                                    {/block}
                                {/if}

                                {block name='layout-header-include-include-categories-mega'}
                                    {include file='snippets/categories_mega.tpl'}
                                {/block}
                            {/navbarnav}
                        </div>
                    </div>
                {/navbar}
            </div>

            {block name='layout-header-search-fixed'}
                {if $Einstellungen.template.theme.mobile_search_type === 'fixed'}
                    <div class="d-lg-none search-form-wrapper-fixed container-fluid container-fluid-xl order-1">
                        {include file='snippets/search_form.tpl' id='search-header-mobile-top'}
                    </div>
                {/if}
            {/block}
        {/if}
    </header>
    {block name='layout-header-search'}
        {if $Einstellungen.template.theme.mobile_search_type === 'fixed' && $isMobile}
            <div class="container-fluid container-fluid-xl fixed-search fixed-top smoothscroll-top-search d-lg-none d-none">
                {include file='snippets/search_form.tpl' id='search-header-mobile-fixed'}
            </div>
        {/if}
    {/block}
    {if $menuScroll}
        {block name='layout-header-jtl-header-scripts'}
            {inline_script}
                <script>
                    let lastScroll = 0,
                        timeoutSc,
                        $navbar = $('.hide-navbar'),
                        $topbar = $('#header-top-bar'),
                        $home   = $('.nav-home-button'),
                        scrollTopActive = false;
                    $(document).on('scroll wheel', function (e) {
                        if (window.innerWidth < globals.breakpoints.lg || $('.secure-checkout-topbar').length) {
                            return;
                        }
                        window.clearTimeout(timeoutSc);
                        timeoutSc = window.setTimeout(function () {
                            let newScroll = $(this).scrollTop();
                            if (newScroll < lastScroll || $(window).scrollTop() === 0) {
                                if ($(window).scrollTop() === 0 && (lastScroll > 100 || e.type === 'wheel' || scrollTopActive)) {
                                    $topbar.addClass('d-lg-flex');
                                    $navbar.removeClass('d-none');
                                    $home.removeClass('d-lg-block');
                                    scrollTopActive = false;
                                } else {
                                    $topbar.removeClass('d-lg-flex');
                                    $navbar.addClass('d-none');
                                    $home.addClass('d-lg-block');
                                }
                            } else {
                                $topbar.removeClass('d-lg-flex');
                                $navbar.addClass('d-none');
                                $home.addClass('d-lg-block');
                            }
                            lastScroll = newScroll;
                        }, 20);
                    });
                    $('.smoothscroll-top').on('click', function () {
                        scrollTopActive = true;
                    });
                </script>
            {/inline_script}
        {/block}
    {/if}
{/block}

{/if}